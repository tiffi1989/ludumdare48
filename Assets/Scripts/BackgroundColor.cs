using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColor : MonoBehaviour
{
    private SpriteRenderer renderer;
    private GameObject player;
    private int counter;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        player = GameObject.Find("Player");

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        counter++;
        if (counter > 10)
        {
            renderer.color = new Color(1, (5000 + player.transform.position.y) / 5000, (5000 + player.transform.position.y) / 5000);
            counter = 0;
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightController : MonoBehaviour
{

    public Light2D playerLight;

    private Light2D globalLight;
    // Start is called before the first frame update
    void Start()
    {
        globalLight = GetComponent<Light2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float playerY = playerLight.gameObject.transform.position.y;
        globalLight.intensity = 1f - Mathf.Abs(playerY) / 1000f;
        if (playerLight.intensity < 1)
            playerLight.intensity = Mathf.Abs(playerY) / 1000f;
    }
}

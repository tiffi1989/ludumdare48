using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{

    private GameObject player;
    private float halfWidth, halfHeight;
    private float counter;
    public float spawnRate;
    public GameObject worm;
    public GameObject lightStripes;

    private List<GameObject> objects = new List<GameObject>();

    public GameObject staticObs;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        Camera camera = Camera.main;
        halfHeight = camera.orthographicSize;
        halfWidth = camera.aspect * halfHeight;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        counter += Time.deltaTime;
        if (counter > 1)
        {
            counter = 0;
            float ran = Random.Range(player.transform.position.y, 0);
            if (Mathf.Sqrt(Mathf.Abs(ran)) > spawnRate)
            {
                if (Random.Range(0f, 8f) < 7)
                    objects.Add(Instantiate(staticObs, new Vector3(Random.Range(-halfWidth, halfWidth), player.transform.position.y - 2 * halfHeight), Quaternion.identity));
                else
                {

                }
                Instantiate(worm, new Vector3(0, player.transform.position.y - 2 * halfHeight), Quaternion.identity);
            }

            if (Random.Range(0, 10) > 8)
            {
                Instantiate(lightStripes, new Vector3(0, player.transform.position.y - 2 * halfHeight), Quaternion.identity);

            }


        }
    }
}

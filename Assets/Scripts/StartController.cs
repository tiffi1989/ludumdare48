using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartController : MonoBehaviour
{

    public Text name;
    public Highscore highscore;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartGame()
    {
        if (name.text.Length > 0)
        {
            highscore.playerName = name.text;
            SceneManager.LoadScene("Main");

        }
    }
}

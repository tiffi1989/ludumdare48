using UnityEngine;


public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        Debug.Log(wrapper.ToString());
        return wrapper.scores;
    }


    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] scores;
    }
}
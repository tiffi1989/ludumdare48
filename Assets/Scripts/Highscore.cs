using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Highscore : MonoBehaviour
{

    public int highscore = 0;
    public string playerName;
    private Text text;

    private float deepestPoint = 0;

    private float pointTimer;

    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!text)
        {
            GameObject textHolder = GameObject.Find("HighscoreText");
            if (textHolder)
                text = textHolder.GetComponent<Text>();
        }
        if (!player)
        {
            player = GameObject.Find("Player");

        }
        pointTimer += Time.deltaTime;
        if (pointTimer > 1f && player)
        {
            float depths = Mathf.Abs(player.transform.position.y) - Mathf.Abs(deepestPoint);
            if (depths > 0)
            {
                deepestPoint = player.transform.position.y;
                highscore += (int)depths;

            }
            highscore--;
            pointTimer = 0;
        }
        if (text)
            text.text = highscore.ToString();
    }

    public void AddToHighscore(int score)
    {
        highscore += score;
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
}

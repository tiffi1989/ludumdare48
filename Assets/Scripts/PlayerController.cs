using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed, maxSpeed;
    public float maxAngle, rotateSpeed;
    private Rigidbody2D rig;

    public int health;

    private float initialScale;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        initialScale = transform.localScale.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
        CheckDeath();
    }

    void Move()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector2 velocity = rig.velocity;
        if (velocity.magnitude < maxSpeed && (x != 0 || y != 0))
        {
            rig.AddForce(new Vector2(x, y / 2).normalized * speed);
        }
        if (velocity.x < 0.1f)
            transform.localScale = new Vector3(-1 * initialScale, 1 * initialScale, 1);
        if (velocity.x > 0.1f)
            transform.localScale = new Vector3(1 * initialScale, 1 * initialScale, 1);

    }

    void CheckDeath()
    {
        if (health <= 0)
        {
            SceneManager.LoadScene("Highscore");
        }
    }
}

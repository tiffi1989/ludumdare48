using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{

    public GameObject[] backgrounds = new GameObject[3];
    private GameObject player;
    public GameObject backgroundPrefab;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player.transform.position.y <= backgrounds[1].transform.position.y)
        {
            GameObject.Destroy(backgrounds[0]);
            backgrounds[0] = backgrounds[1];
            backgrounds[1] = backgrounds[2];
            backgrounds[2] = Instantiate(backgroundPrefab, new Vector3(0, backgrounds[1].transform.position.y - 80), Quaternion.identity);
        }
    }
}
